<?php
namespace Rakuten;
use RakutenRws_Client;


class ItemFacade
{
    private $SDK;
	public $Redis;
    private $redisDomain;
	private $shopCode;
    public $Item;

	public $totalItemCount;
	public $minPrice;
	public $maxPrice;

	public function __construct($shop_code)
	{
		$this->SDK = new RakutenRws_Client;
		$this->SDK->setApplicationId(AsSdk::MY_RAKUTEN_ID);
		$this->SDK->setAffiliateId(AsSdk::MY_RAKUTEN_AFFI);

		$this->shopCode = $shop_code;
		$this->_searchMinPrice();
		$this->_searchMaxPrice();

		$this->Redis = new \Redis();
		$this->Redis->connect("127.0.0.1",6379);
	}

	public function _setRedisDomain($memberDomain=null, $memberId=0)
	{
		$redisDomain = "{$memberDomain}_{$memberId}";
		$this->redisDomain = ($memberId==0) ? null : $redisDomain;
	}
	public function getRedisDomain()
	{
		return $this->redisDomain;
	}

	public function getAllItems($range = 30000)
	{
//        $Redis = new \Redis();
		try {
			$valids = array('shopCode', 'totalItemCount', 'minPrice', 'maxPrice');
			foreach ($valids as $must)
			{
				if(is_null($this->{$must}))
					return $must. ' Not Valids!!!';
			}
//			$objQuery =& SC_Query_Ex::getSingletonInstance();

			$_min = $this->minPrice;
			error_log('minPrice is '.$_min);
			$progress_count = 0;
			do{
				error_log('minRange is '.$_min);
				$progress_count++;
				$page = 1;
				$_max = $_min + $range;
				$options = array(
					'shopCode'      => $this->shopCode,
					'availability'  => 1,
					'imageFlag'		=> 1,
					'sort'          => '+itemPrice',
					'hits'          => 30,
					'page'          => $page,
					'minPrice'      => $_min,
					'maxPrice'      => $_max,
				);
				sleep(1);
				// TODO:: Mod - COUNT
				$response = $this->SDK->execute('IchibaItemSearch', $options);
				$response = $this->_chkResponse($response);
//				if($response['count'] > 3000)
//				{
//					$range = (int)$range;
//					$range /= 2;
//					continue;
//				}
				foreach( $response['Items'] as $data )
				{
					// 登録済みかどうかの判定
					if (!$this->Redis->exists($data['Item']['itemCode']))
					{
						$this->Redis->sAdd($data['Item']['itemCode'], $data['Item']);

                        $this->Item = new Item($data['Item']);
                        yield $this;
					}
				}
				// 複数ページの場合
				if( $response['pageCount'] > 1 )
				{
					$pages = range(2, $response['pageCount']);
					foreach($pages as $p)
					{
//                        error_log($p);
						$options['page'] = $p;
						sleep(1);
						$response = $this->SDK->execute('IchibaItemSearch', $options);
						$this->_chkResponse($response);
						foreach( $response['Items'] as $data )
						{
							if (!$this->Redis->exists($data['Item']['itemCode']))
							{
								$this->Redis->sAdd($data['Item']['itemCode'], $data['Item']);

                                $this->Item = new Item($data['Item']);
                                yield $this;
							}
						}
					}
				}
				$_min = $_max + 1;
				$_max = $_min + $range - 1;
				error_log('maxRange is '.$_max);
				error_log('nextMinRange is '.$_min);
				if($_max > $this->maxPrice) $_max = $this->maxPrice;
				//            $objQuery->update('wcs_batch_cue', array('batch_id' => $batch_id, 'progress' => $progress_count, 'result' => 'progress'));
			}
			while( $_min < $this->maxPrice );

			error_log('starting Update');
			exec("nohup /usr/local/bin/php -d memory_limit=256M ".HTML_REALDIR."batch/update_product_detail.php ".SHOP_DOMAIN.' 7200 > /dev/null 2>&1 &');
			//        $objQuery->update('wcs_batch_cue', array('id' => $batch_id, 'result' => 'finished'));

		} catch( Exception $e ) {
			error_log('Exception!!');
			error_log($e->getMessage());
//            $objQuery->update('wcs_batch_cue', array('batch_id' => $batch_id, 'result' => 'Exception failed'));
		}
	}
	private function _searchMinPrice()
	{
		$options = array(
			'shopCode'		 => $this->shopCode,
			'availability'	 => 1,
			'imageFlag'		 => 1,
			'hits'	    	 => 1,
			'sort'			 => '+itemPrice',
		);
		sleep(1);
		$response = $this->SDK->execute('IchibaItemSearch', $options);
		self::_chkResponse($response);

		$this->totalItemCount = (is_null($this->totalItemCount)) ? $response['count'] : $this->totalItemCount;
		$item = array_shift($response['Items']);
		$this->minPrice = $item['Item']['itemPrice'];

		return true;
	}
	private function _searchMaxPrice()
	{
		$options = array(
			'shopCode'		 => $this->shopCode,
			'availability'	 => 1,
			'imageFlag'		 => 1,
			'hits'	    	 => 1,
			'sort'			 => '-itemPrice',
		);

		sleep(1);
		$response = $this->SDK->execute('IchibaItemSearch', $options);

		self::_chkResponse($response);

		$this->totalItemCount = (is_null($this->totalItemCount)) ? $response['count'] : $this->totalItemCount;
		$item = array_shift($response['Items']);
		$this->maxPrice = $item['Item']['itemPrice'];

		return true;
	}
	private function _chkResponse($response)
	{
		// レスポンスが正しいかを isOk() で確認することができます
		if ($response->isOk()) {
			return $response;
		} else {
			return 'Error:'.$response->getMessage();
		}
	}

}

