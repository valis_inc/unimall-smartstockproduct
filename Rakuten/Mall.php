<?php
namespace Rakuten;


class Mall extends Controller implements AsSdk
{
    use intoDB, intoCSV;
    private $Item;
    public $facade;
    private $masterList = array();
//    private $SDK;
    private $params = array();

    public function __construct(ItemFacade $itemFacade=null)
    {
        $this->facade = $itemFacade;
    }

    public function setItem(Item $Item)
    {
        $this->facade->Item = $Item;
        $this->facade->Item = $Item;
    }
    public function setParams($column='itemName', $data)
    {
        $this->params[$column] = $data;
    }
    public function setMasterList($category='brand', $list)
    {
        $this->masterList[$category] = $list;
    }

    public function categorize($column='itemName', &$insertData=null)
    {
        switch ($column)
        {
            case 'itemName':
                $splitKeysAllWord = $this->facade->Redis->get($this->facade->getRedisDomain().':splitKey:AllWord');
                // STEP1-1: 切り取り箇所をマーキングして返す
                $details = $this->rktSplit($splitKeysAllWord, $this->facade->Item->itemName, 'step2');
                // STEP1-2: マーキング文字で配列変換
                $details = array_filter($details);
                $details = array_values($details);
                $this->facade->Item->trimmed_{$column} = $details;
                $details_origin = $details;
                break;
            case 'itemCaption':
                // STEP1-1: 切り取り箇所をマーキングして返す
                $cap = $this->facade->Item->itemCaption;
                foreach($this->params['itemCaption'] as $data)
                {
                    $cap = $this->rktReplace($cap, $data, 'step1');
                }
                // STEP1-2: マーキング文字で配列変換
                $details = explode(self::$separator['word'], $cap);
                $details = array_filter($details);
                $this->facade->Item->trimmed_{$column} = $details;
                $details_origin = $details;
                break;
        }

        foreach($this->params[$column] as $data)
        {
            $category = $data['input_require']['keywords'];
            // STEP2-1: フォーマット別に置換
            switch ($data['input_require']['format'])
            {
                case 'text':
                case 'textarea':
                    if($column=='itemCaption')
                    {
                        $index = $repeat = 1;
                        foreach($this->facade->Item->trimmed_{$column} as $key => $dt)
                        {
                            $_cnt = 0;
                            $_word = $this->rktReplace($repeat, $data, 'step2'); //! $_word = pregPatternを返す
                            $dt2 = preg_replace($_word, "$1", $dt, -1, $_cnt);
                            if ($_cnt==count($_word))
                            {
                                if($index==$repeat)
                                {
                                    $dt2 = preg_replace("/^¥s(.+)?¥s$/", "$1", $dt2);
                                    $insertData[$data['id']] = array(
//												'product_id' => ,
                                        'mtb_pd_id' => $data['id'],
                                        'content' => $dt2
                                    );
                                    // カテゴリ登録
                                }
                                $index++;
                            }
                        }

                    }
                    break;
                case 'master':
                    if(!$category)return false;
                    // TODO::not objQuery
                    if($data['input_require']['keywords']=='item')
                    {
                        $_category = $this->facede->Item->trimmed_{$column} = Smst::getCategory($this->facade->Item->genreId);
                        $splitKeysAllWord = $this->facade->Redis->get($this->facade->getRedisDomain().':splitKey:AllWord');
                        $_category = array_filter($_category);
                        $_category = array_values($_category);
                        $this->facade->Item->trimmed_{$column} = $_category;
                    }
                    else
                    {
                        $this->facade->Item->trimmed_{$column} = $details_origin;
                    }
                    if($column=='itemName')
                    {
                        foreach ($this->facade->Item->trimmed_{$column} as $_master)
                        {
                            $data['name'] = $_master;
                            $shortest = -1; // マッチの最短距離初期値
                            if($data['name'] && !$data['delete'])
                            {
                                $dt2 = $data['name'];
                                $_data_x = (mb_strlen($dt2)==strlen($dt2) && $data['input_require']['keywords']=='brand') ? 'data2' : 'data1'; // マルチバイト判定
                                foreach ($this->masterList[$category] as $_id => $record)
                                {
                                    if($record[$_data_x])
                                    {
                                        $lev = levenshtein($dt2, $record[$_data_x]);
                                        if ($lev == 0)
                                        {
                                            $shortest = 0;
                                            $insertData[$data['id']] = array(
//												'product_id' => ,
                                                'mtb_pd_id' => $data['id'],
                                                'content' => $record['id'],
                                                'shortest' => $shortest
                                            );
                                            break 2;
                                        }
                                        if ($lev <= $shortest || $shortest < 0)
                                        {
                                            // 最短のマッチと最短距離をセットします
                                            $shortest = $lev;
                                            if(!$insertData[$data['id']] || ($insertData[$data['id']]['shortest']>$shortest && $shortest!=-1))
                                            {
                                                $insertData[$data['id']] = array(
    //												'product_id' => $product_id,
                                                    'mtb_pd_id' => $data['id'],
                                                    'content' => $record['id'],
                                                    'shortest' => $shortest
                                                );
                                            }
//                                            $insertData = array(
////												'product_id' => $product_id,
//                                                'mtb_pd_id' => $data['id'],
//                                                'content' => $record['id']
//                                            );
                                        }
                                        if($_data_x=='data2')
                                        {
                                            $oneliner = preg_replace("/( |　)/", "",$data['Item']['itemName']);
                                            $_data2 = preg_replace("/( |　)/", "",$record['data2']);
                                            if(strpos($oneliner, $_data2)!==false)
                                            {
                                                $english_flg = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(($shortest<3 && $shortest >= 0) || $english_flg)
                        {
                            if(in_array($data['id'], $this->masterList['must_id']))$ins_val['status'] = 1;
                            $lastShorter[$data['id']] = $shortest;
                            if(!$insertData[$data['id']] || ($insertData[$data['id']]['shortest']>$shortest && $shortest!=-1))
                            {
                                $insertData[$data['id']] = array(
    //												'product_id' => $product_id,
                                    'mtb_pd_id' => $data['id'],
                                    'content' => $record['id'],
                                    'shortest' => $shortest
                                );
                            }
//                            $insertData = array(
////												'product_id' => $product_id,
//                                'mtb_pd_id' => $data['id'],
//                                'content' => $record['id']
//                            );
                        }
                    }
                    if($column=='itemCaption')
                    {
                        $index = $repeat = 1;
                        foreach($this->facade->Item->trimmed_{$column} as $key => $dt)
                        {
                            $last_id = null;
                            $shortest = -1; // マッチの最短距離初期値
                            $_cnt = 0;
                            $_word = $this->rktReplace($repeat, $data, 'step2'); //! $_word = pregPatternを返す
                            $dt2 = preg_replace($_word, "$1", $dt, -1, $_cnt);
                            if ($_cnt==count($_word) && !$data['delete'])
                            {
                                $_data_x = (mb_strlen($dt2)==strlen($dt2) && $data['input_require']['keywords']=='brand') ? 'data2' : 'data1'; // マルチバイト判定
                                foreach ($this->masterList[$category] as $_id => $record)
                                {
                                    $lev = levenshtein($dt2, $record[$_data_x]);
//                                    if ($lev == 0 && $last_id!=$record['id'])
                                    if ($lev == 0)
                                    {
                                        $shortest = 0;
                                        if(!$insertData[$data['id']] || ($insertData[$data['id']]['shortest']>$shortest && $shortest!=-1))
                                        {
                                            $insertData[$data['id']] = array(
    //											'product_id' => $product_id,
                                                'mtb_pd_id' => $data['id'],
                                                'content' => $record['id'],
                                                'shortest' => $shortest
                                            );
                                        }
//                                        $last_id = $record['id'];
                                        break 2;
                                    }
                                    if ($lev <= $shortest || $shortest < 0)
                                    {
                                        // 最短のマッチと最短距離をセットします
                                        $shortest = $lev;
                                        if(!$insertData[$data['id']] || ($insertData[$data['id']]['shortest']>$shortest && $shortest!=-1))
                                        {
                                            $insertData[$data['id']] = array(
    //											'product_id' => $product_id,
                                                'mtb_pd_id' => $data['id'],
                                                'content' => $record['id'],
                                                'shortest' => $shortest
                                            );
                                        }
                                    }
                                }
                            }
//                            if($insertData[$data['id']])
                            if($insertData['content'])
                            {
                                if(isset($lastShorter) && $lastShorter[$data['id']] > $shortest && $shortest > 0)
                                {
                                    $lastShorter[$data['id']] = $shortest;
                                    if(in_array($data['id'], $this->masterList['must_id']))$ins_val['status'] = 1;
                                    if(!$insertData[$data['id']] || ($insertData[$data['id']]['shortest']>$shortest && $shortest!=-1))
                                    {
                                        $insertData[$data['id']] = array(
//												'product_id' => $product_id,
                                        'mtb_pd_id' => $data['id'],
                                        'content' => $record['id'],
                                        'shortest' => $shortest
                                    );
                                    }
                                }
                            }
                        }
                        break;
                    }
                    break;
                case 'checkbox_multi':
                    break;
                case 'date':
                case 'datetime':
//                    $insertData[$data['id']] = array(
//                        'mtb_pd_id' => $data['id'],
//                        'content' => date('Y-m-d H:i:s')
//                    );
                    break;
                case 'radio':
                    break;

            }
        }
        return $insertData;

    }


    /**
     * itemUrl=product_code切り出し
     *
     * @param string $itemUrl
     * @return string $url
     */
    public function extProductCode($itemUrl)
    {
        try{
            $trim = '#\?pc=(http.+)&m=.+?$#';
            $itemUrl = preg_replace($trim, "$1", $itemUrl);
            $ptn = '#\/|%2F#';
            $urlArray = preg_split($ptn, $itemUrl);
            $urlArray = array_filter($urlArray);
            $url = array_pop($urlArray);
            return $url;
        }catch( Exception $e){
            return false;
        }
    }


}
