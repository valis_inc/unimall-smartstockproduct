<?php
namespace Rakuten;


trait intoDB
{
    private $tmpDB;
    public function _setDB($dbobj)
    {
        $this->tmpDB = $dbobj;
    }
    public function _insertEachTable($data)
    {
        $objQuery = $this->tmpDB;

        $product_id = $objQuery->nextVal('dtb_products_product_id');
        $product_cls_id = $objQuery->nextVal('dtb_products_class_product_class_id');
        /**
         * TABLE VARIABLES
         */
        /** @var array $ins_val  商品テーブル用 dtb_products　*/
        $ins_val = array(
            'product_id' => $product_id,
            'name' => $data['Item']['itemName'],
            'status' => 1,
            'main_comment' => $data['Item']['itemCaption'],
            'creator_id' => $this->_postData['member_id']
        );
        /** @var array $ins_val_ss_category  モール別カテゴリ dtb_ss_product_category　*/
        $ins_val_ss_category = array(
//                        'id' => $objQuery->nextVal('dtb_ss_product_category'),
            'product_id' => $product_id,
            'shop_id' => 1, // 楽天
            'category_data' => $data['Item']['genreId']
        );
        /** @var array $ins_val_ss_category  店内カテゴリ dtb_product_categories　*/
//                    $ins_val_categories = array(
//                        'product_id' => $product_id,
//                        'shop_id' => 1, // 楽天
//                        'category_data' => $data['Item']['genreId']
//                    );
        /** @var array $ins_val_cls  商品規格用 dtb_products_class　*/
        // 商品規格
        $ins_val_cls = array(
            'product_class_id'  => $product_cls_id,
            'product_id'        => $product_id,
//                        'classcategory_id1' => 0,
//                        'classcategory_id2' => 0,
            'product_type_id'   => 1,
            'down_filename'      => $data['Item']['itemCode'],
//            'product_code'      => $data['Item']['itemCode'],
            'product_code'      => $this->_postData['member_id'].'-'.$this->Controller->extProductCode($data['Item']['itemUrl']),
            'stock'             => 1,
//                        'stock_unlimited'   => NULL,
//                        'sale_limit'        => NULL,
            'price01'           => $data['Item']['itemPrice'],
            'price02'           => $data['Item']['itemPrice'],
            'deliv_fee'         => $data['Item']['postageFlag'], // 0:送料込み 1:送料別
            'point_rate'        => $data['Item']['pointRate'],
            'creator_id' => $this->_postData['member_id'],
            'create_date'       => date('Y-m-d H:i:s'),
            'update_date'       => date('Y-m-d H:i:s')
        );
        /** @var array $ins_val_disp モール別表示非表示 wcs_shop_disp*/
        $ins_val_disp = array(
            "product_id" => $product_id,
            "shop_disp " => ',1,2,3,4,5,6,7,8,9,10,'
        );
        /** @var array $ins_val_ss_product_status オークション状況テーブル wcs_ss_product_status */
//                    $ins_val_ss_product_status = array(
//                        "id" => '',
//                        "product_code" => $data['Item']['itemCode'],
//                        "shop_id" => '1', // 楽天
//                        "ss_id" => $data['Item']['itemCode'],
//                        "url" => $data['Item']['itemUrl'],
//                        "now_price" => $data['Item']['itemPrice'],
//                        'name' => $data['Item']['itemName']
//                    );

//                    $ins_val_order = array(
//                        'order_detail_id'   => $order_detail_id,
//                        'order_id'          => $order_id,
//                        'product_id'        => $product_id,
//                        'product_class_id'  => $product_cls_id,
//                        'product_name'      => $data['Item']['itemName'],
//                        'product_code'      => $data['Item']['itemCode']
//                    );

        // 画像挿入
        $L_images = (is_array($data['Item']['mediumImageUrls'])) ? $data['Item']['mediumImageUrls'] : array();
//        $S_images = (is_array($data['Item']['smallImageUrls'])) ? $data['Item']['smallImageUrls'] : array();
        $L_urls = array();
        foreach($L_images as $key => $img)
        {
            // 画像URL 成型
            $L_urls[] = preg_replace('/^.*?(http.+)(\?.+=.+$)/', "$1", $img['imageUrl']);
        }
        $ins_val['main_image'] = implode("\n", $L_urls);
        $ins_val['main_list_image'] = $L_urls[0];
//        $ins_val['main_image'] = serialize($S_images[0]);
//        $ins_val['main_image'] = serialize(implode("¥n", $S_images));

        // タイトル・キャプション保存
        $objQuery->begin();
        $objQuery->insert('dtb_ss_product_category', $ins_val_ss_category);
        $objQuery->insert('dtb_products_class', $ins_val_cls);
        $objQuery->insert('wcs_shop_disp', $ins_val_disp);
//                    $objQuery->insert('wcs_ss_product_status', $ins_val_ss_product_status);
//                    $objQuery->insert('dtb_order_detail', $ins_val_order);
        $objQuery->commit();
        unset($ins_val_cls);


        // キーワード毎に切り取り
        $details = $result = array();
        $_keys = $this->Controller->separator;
        $_key = $_keys['word'];
        static $types = array('itemName', 'itemCaption');
        foreach ($types as $type)
        {
            switch ($type)
            {
                case 'itemName':
                    $splitKeysAllWord = $this->Redis->get($this->getRedisDomain().':splitKey:AllWord');
                    // STEP1-1: 切り取り箇所をマーキングして返す
                    $details = $this->Controller->rktSplit($splitKeysAllWord, $data['Item']['itemName'], 'step2');
                    // STEP1-2: マーキング文字で配列変換
                    $details = array_filter($details);
                    $details = array_values($details);
                    $details_origin = $details;
                    break;
                case 'itemCaption':
                    // STEP1-1: 切り取り箇所をマーキングして返す
                    $cap = $data['Item'][$type];
                    foreach($this->_postData['detail'][$type] as $pd)
                    {
                        $cap = $this->Controller->rktReplace($cap, $pd, 'step1');
                    }
                    // STEP1-2: マーキング文字で配列変換
                    $details = explode($_key, $cap);
                    $details = array_filter($details);
                    $details_origin = $details;
                    break;
            }
            // STEP2-1: フォーマット別に置換
            foreach($this->_postData['detail'][$type] as $pd)
            {
                // STEP2-2A: マーキングと同条件の対象文字が含まれているかで検定
                if($pd['input_require']['format']=='text')
                {
                    switch($type)
                    {
                        case 'itemName':
                            $pd['name'] = (int)$pd['name']-1;
                            if($pd['name'] >= 0)
                            {
                                $dt2 = $details[$pd['name']];
                                $ins_val_dt[$pd['id']] = array(
//                                    'id' => $detail_id,
                                    'product_id' => $product_id,
                                    'mtb_pd_id' => $pd['id'],
                                    'content' => $dt2
                                );
                                $objQuery->begin();
                                $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
                                $objQuery->commit();
                                // カテゴリ登録
                                $this->applyProductCategory($ins_val_dt);
                            }
                            break;
                        case 'itemCaption':
                            $index = $repeat = 1;
                            foreach($details as $key => $dt)
                            {
                                $_cnt = 0;
                                $_word = $this->Controller->rktReplace($repeat, $pd, 'step2'); //! $_word = pregPatternを返す
                                $dt2 = preg_replace($_word, "$1", $dt, -1, $_cnt);
                                if ($_cnt==count($_word))
                                {
                                    if($index==$repeat)
                                    {
//                                $detail_id = $objQuery->nextVal('wcs_products_detail'); // autoIncrement
                                        $dt2 = preg_replace("/^¥s(.+)?¥s$/", "$1", $dt2);
                                        $ins_val_dt[$pd['id']] = array(
//                                    'id' => $detail_id,
                                            'product_id' => $product_id,
                                            'mtb_pd_id' => $pd['id'],
                                            'content' => $dt2
                                        );
                                        $objQuery->begin();
                                        $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
                                        $objQuery->commit();
                                        // カテゴリ登録
                                        $this->applyProductCategory($ins_val_dt);
                                    }
                                    $index++;
                                }
                            }
                            break;
                    }
                    // 商品名を表示用商品名のデフォルトに
                    if($pd['name']=='表示用商品名')
                    {
                        $detail_id = $objQuery->nextVal('wcs_products_detail');
                        $ins_val_dt[$pd['id']] = array(
//			                'id' => $detail_id,
                            'product_id' => $product_id,
                            'mtb_pd_id' => $pd['id'],
                            'content' => $data['Item']['itemName']
                        );
                        $objQuery->begin();
                        $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
                        $objQuery->commit();
                    }
                }
                // STEP2-2C: 日付
                elseif($pd['input_require']['format']=='datetime')
                {
                    $ins_val_dt[$pd['id']] = array(
                        'product_id' => $product_id,
                        'mtb_pd_id' => $pd['id'],
                        'content' => date('Y-m-d H:i:s')
                    );
                    $objQuery->begin();
                    $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
                    $objQuery->commit();
                    // カテゴリ登録
                    $this->applyProductCategory($ins_val_dt);
                }
                // STEP2-2B: ブランドマスタから選択 かつ 最短マッチ検索
                elseif ($pd['input_require']['format']=='master')
                {
                    $_master_list = $objQuery->getAll('SELECT * FROM wcs_master WHERE category =\'' .$pd['input_require']['keywords']. '\'');

                    // 必須フラグ - ブランドが取得できなかった場合 status=0 に
                    $_must_id = $objQuery->getAll("SELECT id FROM wcs_mtb_product_detail WHERE name LIKE 'ブランド'");
                    if(in_array($pd['id'], $_must_id))$ins_val['status']=0;
                    $ins_val_dt[$pd['id']] = null;
                    // Item判定
                    if($pd['input_require']['keywords']=='item'){
                        $details_tmp = $objQuery->getRow('category1, category2, category3, category4, category5, category6, category7, category8, category9, category10', 'wcs_ss_category', "category_id = ".$data['Item']['genreId']);
                        foreach($details_tmp as $dt)
                        {
                            $dt = explode('・', $dt);
                            if(is_array($dt)){
                                foreach($dt as $d)$details[] = $d;
                            }else{
                                $details_master[] = $dt;
                            }
                        }
                    }else{
                        $details = $details_origin;
                    }
                    switch($type)
                    {
                        case 'itemName':
                            foreach($details as $_master)
                            {
                                $pd['name'] = $_master;
                                $shortest = -1; // マッチの最短距離初期値
                                if($pd['name'] && !$pd['delete'])
                                {
                                    $dt2 = $pd['name'];
                                    $_data_x = (mb_strlen($dt2)==strlen($dt2) && $pd['input_require']['keywords']=='brand') ? 'data2' : 'data1'; // マルチバイト判定
                                    foreach ($_master_list as $_id => $record)
                                    {
                                        if($record[$_data_x])
                                        {
                                            $lev = levenshtein($dt2, $record[$_data_x]);
                                            if ($lev == 0)
                                            {
                                                $ins_val_dt[$pd['id']] = array(
//                                'id' => $detail_id,
                                                    'product_id' => $product_id,
                                                    'mtb_pd_id' => $pd['id'],
                                                    'content' => $record['id']
                                                );
                                                $shortest = 0;
                                                break 2;
                                            }
                                            if ($lev <= $shortest || $shortest < 0)
                                            {
                                                // 最短のマッチと最短距離をセットします
                                                $ins_val_dt[$pd['id']] = array(
//                                'id' => $detail_id,
                                                    'product_id' => $product_id,
                                                    'mtb_pd_id' => $pd['id'],
                                                    'content' => $record['id']
                                                );
                                                $shortest = $lev;
                                            }
                                            if($_data_x=='data2')
                                            {
                                                $oneliner = preg_replace("/( |　)/", "",$data['Item']['itemName']);
                                                $_data2 = preg_replace("/( |　)/", "",$record['data2']);
                                                if(strpos($oneliner, $_data2)!==false)
                                                {
                                                    $english_flg = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            error_log('shortest is ...'.$shortest);
                            if(($shortest<3 && $shortest >= 0) || $english_flg)
                            {
                                $lastShorter[$pd['id']] = $shortest;
                                if(in_array($pd['id'], $_must_id))$ins_val['status']=1;
                                $objQuery->begin();
                                $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
                                $objQuery->commit();
                                // カテゴリ登録
                                $this->applyProductCategory($ins_val_dt);
                            }
                            break;
                        case 'itemCaption':
                            $index = $repeat = 1;
                            foreach($details as $key => $dt)
                            {
                                $shortest = -1; // マッチの最短距離初期値
                                $_cnt = 0;
                                $_word = $this->Controller->rktReplace($repeat, $pd, 'step2'); //! $_word = pregPatternを返す
                                $dt2 = preg_replace($_word, "$1", $dt, -1, $_cnt);
                                if ($_cnt==count($_word) && !$pd['delete'])
                                {
                                    $_data_x = (mb_strlen($dt2)==strlen($dt2) && $pd['input_require']['keywords']=='brand') ? 'data2' : 'data1'; // マルチバイト判定
                                    foreach ($_master_list as $_id => $record)
                                    {
                                        $lev = levenshtein($dt2, $record[$_data_x]);
                                        if ($lev == 0)
                                        {
                                            $ins_val_dt[$pd['id']] = array(
//                                'id' => $detail_id,
                                                'product_id' => $product_id,
                                                'mtb_pd_id' => $pd['id'],
                                                'content' => $record['id']
                                            );
                                            $shortest = 0;
                                            break 2;
                                        }
                                        if ($lev <= $shortest || $shortest < 0)
                                        {
                                            // 最短のマッチと最短距離をセットします
                                            $ins_val_dt[$pd['id']] = array(
//                                'id' => $detail_id,
                                                'product_id' => $product_id,
                                                'mtb_pd_id' => $pd['id'],
                                                'content' => $record['id']
                                            );
                                            $shortest = $lev;
                                        }
                                    }
                                }
                                if($ins_val_dt[$pd['id']])
                                {
                                    if(isset($lastShorter) && $lastShorter[$pd['id']] > $shortest && $shortest > 0)
                                    {
                                        $lastShorter[$pd['id']] = $shortest;
                                        if(in_array($pd['id'], $_must_id))$ins_val['status'] = 1;
                                        $objQuery->begin();
                                        $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
                                        $objQuery->commit();
                                        // カテゴリ登録
                                        $this->applyProductCategory($ins_val_dt);
                                        break;
                                    }
                                }
                            }
                            break;
                    }
                }
                elseif ($pd['input_require']['format']=='checkbox_multi')
                {
                    $_multi = explode(',', $pd['input_require']['keywords']);
                    $flg = false;
                    foreach($_multi as $_m)
                    {
                        if(strstr($dt, $_m)){
                            $dt2[] = $_m;
                            $flg = true;
                        }
                    }
                    if($flg){
                        $dt2 = implode(',', $dt2);
                        $ins_val_dt[$pd['id']] = array(
                            'product_id' => $product_id,
                            'mtb_pd_id' => $pd['id'],
                            'content' => date('Y-m-d H:i:s')
                        );
                        $objQuery->begin();
                        $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
                        $objQuery->commit();
                    }


                }

                /** TODO:: Radio */
//                elseif ($pd['input_require']['format']=='radio')
//                {
//                    $ins_val_dt[$pd['id']] = null;
//                    switch($type)
//                    {
//                        case 'itemName':
//                            foreach($details as $_master)
//                            {
////                                $pd['name'] = (int)$pd['name']-1;
//                                $pd['name'] = $_master;
////                                error_log($_master);
//                                $shortest = -1; // マッチの最短距離初期値
//                                if($pd['name'])
//                                {
////                                    $dt2 = $details[$pd['name']];
//                                    $dt2 = $pd['name'];
//                                    foreach ($_master_list as $_id => $record)
//                                    {
//                                        $lev = levenshtein($dt2, $record['data1']);
//                                        if ($lev == 0)
//                                        {
//                                            $ins_val_dt[$pd['id']] = array(
////                                'id' => $detail_id,
//                                                'product_id' => $product_id,
//                                                'mtb_pd_id' => $pd['id'],
//                                                'content' => $record['id']
//                                            );
//                                            $shortest = 0;
//                                            break 2;
//                                        }
//                                        if ($lev <= $shortest || $shortest < 0)
//                                        {
//                                            // 最短のマッチと最短距離をセットします
//                                            $ins_val_dt[$pd['id']] = array(
////                                'id' => $detail_id,
//                                                'product_id' => $product_id,
//                                                'mtb_pd_id' => $pd['id'],
//                                                'content' => $record['id']
//                                            );
//                                            $shortest = $lev;
//                                        }
//                                    }
//                                    foreach ($_master_list as $_id => $record)
//                                    {
//                                        $lev = levenshtein($dt2, $record['data2']);
//                                        if ($lev == 0)
//                                        {
//                                            $ins_val_dt[$pd['id']] = array(
////                                'id' => $detail_id,
//                                                'product_id' => $product_id,
//                                                'mtb_pd_id' => $pd['id'],
//                                                'content' => $record['id']
//                                            );
//                                            $shortest = 0;
//                                            break 2;
//                                        }
//                                        if ($lev <= $shortest || $shortest < 0)
//                                        {
//                                            // 最短のマッチと最短距離をセットします
//                                            $ins_val_dt[$pd['id']] = array(
////                                'id' => $detail_id,
//                                                'product_id' => $product_id,
//                                                'mtb_pd_id' => $pd['id'],
//                                                'content' => $record['id']
//                                            );
//                                            $shortest = $lev;
//                                        }
//                                    }
//                                }
//                            }
//                            error_log('shortest is ...'.$shortest);
//                            if($shortest<3 && $shortest >= 0)
//                            {
//                                $objQuery->begin();
//                                $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
//                                $objQuery->commit();
//                                // カテゴリ登録
//                                $this->applyProductCategory($ins_val_dt);
//                            }
//                            break;
//                        case 'itemCaption':
//                            $index = $repeat = 1;
//                            foreach($details as $key => $dt)
//                            {
//                                $shortest = -1; // マッチの最短距離初期値
//                                $_cnt = 0;
//                                $_word = $this->Controller->rktReplace($repeat, $pd, 'step2'); //! $_word = pregPatternを返す
//                                $dt2 = preg_replace($_word, "$1", $dt, -1, $_cnt);
//                                if ($_cnt==count($_word))
//                                {
//                                    foreach ($_master_list as $_id => $record)
//                                    {
//                                        $lev = levenshtein($dt2, $record['data1']);
//                                        if ($lev == 0)
//                                        {
//                                            $ins_val_dt[$pd['id']] = array(
////                                'id' => $detail_id,
//                                                'product_id' => $product_id,
//                                                'mtb_pd_id' => $pd['id'],
//                                                'content' => $record['id']
//                                            );
//                                            $shortest = 0;
//                                        }
//                                        if ($lev <= $shortest || $shortest < 0)
//                                        {
//                                            // 最短のマッチと最短距離をセットします
//                                            $ins_val_dt[$pd['id']] = array(
////                                'id' => $detail_id,
//                                                'product_id' => $product_id,
//                                                'mtb_pd_id' => $pd['id'],
//                                                'content' => $record['id']
//                                            );
//                                            $shortest = $lev;
//                                        }
//                                    }
//                                }
//                                if($ins_val_dt[$pd['id']])
//                                {
//                                    $_must_flg = true;
//                                    $objQuery->begin();
//                                    $objQuery->insert('wcs_product_detail', $ins_val_dt[$pd['id']]);
//                                    $objQuery->commit();
//                                    // カテゴリ登録
//                                    $this->applyProductCategory($ins_val_dt);
//                                    break;
//                                }
//                            }
//                            break;
//                    }
//
//                }
            }
        }
        // タイトル・キャプション保存
        $objQuery->begin();
        $objQuery->insert('dtb_products', $ins_val); // 最後へ移動
        $objQuery->commit();
        unset($ins_val);

//        $objQuery->begin();
//        $objQuery->update('dtb_products', array('product_id' => $product_id, 'update_date' => date('Y-m-d H:i:s'))); // product_details更新用
//        $objQuery->commit();

//        exit;
    }
}
