<?php
namespace Rakuten;
use RakutenRws_Client;


class Item
{
	public $itemName;
	public $trimmed_itemName;
	public $catchcopy;
	public $itemCode;
	public $itemPrice;
	public $itemCaption;
	public $trimmed_itemCaption;
	public $itemUrl;
	public $affiliateUrl;
	public $shopAffiliateUrl;
	public $imageFlag ;
	public $smallImageUrls = array();
	public $mediumImageUrls = array();
	public $availability;
	public $taxFlag;
	public $postageFlag;
	public $creditCardFlag;
	public $shopOfTheYearFlag;
	public $shipOverseasFlag;
	public $shipOverseasArea;
	public $asurakuFlag;
	public $asurakuClosingTime;
	public $asurakuArea;
	public $affiliateRate;
	public $startTime;
	public $endTime;
	public $reviewCount;
	public $reviewAverage;
	public $pointRate;
	public $pointRateStartTime;
	public $pointRateEndTime;
	public $giftFlag;
	public $shopName;
	public $shopCode;
	public $shopUrl;
	public $genreId;
	public $tagIds;

	public function __construct($dataItem)
	{
		if(is_array($dataItem))self::setParams($dataItem);
	}

	public function setParams($dataItem)
	{
		foreach($dataItem as $param => $data)
		{
			$this->$param = $data;
		}
		self::takeImages();
	}
	private function takeImages()
	{
		$L_images = (is_array($this->mediumImageUrls)) ? $this->mediumImageUrls : array();
		$L_urls = array();
		foreach($L_images as $key => $img)
		{
			// 画像URL 成型
			$L_urls[] = preg_replace('/^.*?(http.+)(\?.+=.+$)/', "$1", $img['imageUrl']);
		}
		$this->mediumImageUrls = $L_urls;
//		$ins_val['main_image'] = implode("\n", $L_urls);
//		$ins_val['main_list_image'] = $L_urls[0];

	}
}
