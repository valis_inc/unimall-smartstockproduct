<?php
namespace Rakuten;

class Smst
{
	static $tables = array(
		'dtb_products',
		'dtb_ss_product_category',
		'dtb_products_class',
		'wcs_shop_disp',
		'wcs_product_detail',
	);

	static $formats = array(
		'brand',
		'condition',
		'deliv_fee',
		'item',
		'material',
		'trans2en',
	);

	static $ins_val_dt = array(
//                                    'id' => $detail_id,
		'product_id' => '',//$product_id,
		'mtb_pd_id' => '',//$pd['id'],
		'content' => '',//$dt2
	);

	static $ins_val = array(
		'product_id' => '', //$product_id,
		'name' => '', //$data['Item']['itemName'],
		'status' => 1,
		'main_comment' => '', //$data['Item']['itemCaption'],
		'creator_id' => '', //$this->_postData['member_id']
	);

	static $ins_val_ss_category = array(
//                        'id' => $objQuery->nextVal('dtb_ss_product_category'),
		'product_id' => '', //$product_id,
		'shop_id' => 1, // 楽天
		'category_data' => '', //$data['Item']['genreId']
	);

	static $ins_val_cls = array(
		'product_class_id'  => '', //$product_cls_id,
		'product_id'        => '', //$product_id,
		//                        'classcategory_id1' => 0,
		//                        'classcategory_id2' => 0,
		'product_type_id'   => 1,
		'down_filename'      => '', //$data['Item']['itemCode'],
		//            'product_code'      => $data['Item']['itemCode'],
		'product_code'      => '', //$this->_postData['member_id'].'-'.$this->Controller->extProductCode($data['Item']['itemUrl']),
		'stock'             => 1,
		//                        'stock_unlimited'   => NULL,
		//                        'sale_limit'        => NULL,
		'price01'           => '', //$data['Item']['itemPrice'],
		'price02'           => '', //$data['Item']['itemPrice'],
		'deliv_fee'         => '', //$data['Item']['postageFlag'], // 0:送料込み 1:送料別
		'point_rate'        => '', //$data['Item']['pointRate'],
		'creator_id' => '', //$this->_postData['member_id'],
		'create_date'       => '', //date('Y-m-d H:i:s'),
		'update_date'       => '', //date('Y-m-d H:i:s')
	);

	static $ins_val_disp = array(
		"product_id" => '', //$product_id,
		"shop_disp " => ',1,2,3,4,5,6,7,8,9,10,'
	);

	static public function getCategory($genre_id)
	{
		$_categories = array();
		$objQuery =& \SC_Query_Ex::getSingletonInstance();
		$_category = $objQuery->getRow('category1, category2, category3, category4, category5, category6, category7, category8, category9, category10', 'wcs_ss_category', "category_id = ". $genre_id);
		foreach($_category as $_cat)
		{
			$_cat = explode('・', $_cat);
			if(is_array($_cat)){
				foreach($_cat as $_c)$_categories[] = $_c;
			}else{
				$_categories[] = $_cat;
			}
		}
		return $_categories;
	}

	public function applyProductCategory($ins_val_dt) {

		$objQuery =& \SC_Query_Ex::getSingletonInstance();

		//カテゴリテーブル
		$this->arrCategoryTable = $objQuery->getRow('mtb_pd_id1, mtb_pd_id2, mtb_pd_id3, mtb_pd_id4, mtb_pd_id5, mtb_pd_id6', 'wcs_category_table', 'id > 0');
		//店舗データ
//        $tmp_arrShops = $objQuery->getAll('SELECT * FROM wcs_shops WHERE mtb_ss_id != 12');
//        foreach ($tmp_arrShops as $shop) {
//            $this->arrShops[$shop['id']] = $shop;
//            $this->arrShops[$shop['id']]['data'] = unserialize($shop['data']);
//        }

		$where = array();
		$pd_datas = array();
		foreach ($this->arrCategoryTable as $key=>$value) {
			foreach(array('itemName', 'itemCaption') as $type)
			{
//                if ($this->arrProductDetail[$value]['colum_name']) {
//                    if ($ins_val_dt[$value]['name']) {
//                        $where[] = preg_replace('/mtb_pd_id/', 'pd_data', $key)." = ".$objQuery->quote($this->_postData['detail'][$type][$value]['name']);
//                        $pd_datas[preg_replace('/mtb_pd_id/', 'pd_data', $key)] = $this->_postData['detail'][$type][$value]['name'];
//                    }
//                } else if ($this->_postData['detail'][$type][$value]['name']) {
//                    $where[] = preg_replace('/mtb_pd_id/', 'pd_data', $key)." = ".$objQuery->quote($this->_postData['detail'][$type][$value]['name']);
//                    $pd_datas[preg_replace('/mtb_pd_id/', 'pd_data', $key)] = $this->_postData['detail'][$type][$value]['name'];
//                }
				if($ins_val_dt[$value])
				{
					$where[] = preg_replace('/mtb_pd_id/', 'pd_data', $key)." = ".$objQuery->quote($ins_val_dt[$value]['content']);
					$pd_datas[preg_replace('/mtb_pd_id/', 'pd_data', $key)] = $ins_val_dt[$value]['content'];
					$product_id = $ins_val_dt[$value]['product_id'];
				}
			}
//            if ($this->arrProductDetail[$value]) {
//                $where[] = preg_replace('/mtb_pd_id/', 'pd_data', $key)." = ".$objQuery->quote($this->arrProductDetail[$value]['name']);
//                $pd_datas[preg_replace('/mtb_pd_id/', 'pd_data', $key)] = $this->arrProductDetail[$value]['name'];
//            }
		}


		if (count($where)) {
			$where = array_unique($where);
			//ショップ毎にカテゴリを登録
			foreach ($this->arrShops as $shop) {

				//商品詳細に一致するものを抽出
				$matched_category_list = $objQuery->getAll('SELECT pd_data1, pd_data2, pd_data3, pd_data4, pd_data5, pd_data6, category_data FROM wcs_category_table WHERE shop_id = ? AND ('.implode(' OR ', $where).')', array($shop['id']));

				//適合するカテゴリにスコアをつける
				$matched = array();
				foreach ($matched_category_list as $value) {
					$match_flag = 0;
					for ($i=1; $i<=6; $i++) {
						if ($value['pd_data'.$i]) {
							if ($value['pd_data'.$i] == $pd_datas['pd_data'.$i]) {
								$match_flag++;
							} else {
								$match_flag = 0;
								break;
							}
						}
					}
					if ($match_flag) {
						$matched[] = array('match_score' => $match_flag, 'category_data' => $value['category_data']);
					}
				}
				unset($matched_category_list);

				//スコア順で並び替え
				usort($matched, compare);

				switch ($shop['mtb_ss_id']) {

					case 1:	//ECCubeの場合
					case 2:	//楽天市場の場合
						$category_ids = array();
						foreach ($matched as $value) {
							$rank = $objQuery->getOne('SELECT rank FROM dtb_category WHERE category_id = ?', array($value['category_data']));
							$unique = $objQuery->getOne('SELECT * FROM dtb_product_categories WHERE category_id = ? AND product_id = ?', array($value['category_data'], $product_id));
							error_log($rank);
							if($rank && !$unique){
								$objQuery->insert('dtb_product_categories', array('product_id' => $product_id, 'category_id' => $value['category_data'], 'rank' => $rank));
							}
//                            $category_ids[] = $value['category_data'];
						}
//                        $this->arrForm['category_id'] = $category_ids;
						break;

					case 3:	//ショップサーブの場合
					case 4:	//eBayの場合
					case 5:	//Amazonの場合
					case 6:	//ヤフオクの場合
					case 7:	//タオバオの場合
					case 9:	//ヤフーショッピングの場合
//                        if (count($matched)) {
//                            $this->arrForm['ss_category'.$shop['id']] = $matched[0]['category_data'];
//                        }
						break;
				}
			}
		}
	}

}