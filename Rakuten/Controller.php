<?php
namespace Rakuten;
abstract class Controller
{
    static $separator = array(
        'word' => '%##%', //単語置き換え
        'separate' => '|', //分割
        'repeat' => '*', //繰り返し *2, *4 等
        'bullet' => '^', //行頭
        'end' => '$'
    );

    public function rktSplit(&$splitKeysAll, $splits, $role='step1')
    {
        switch($role)
        {
            case 'exchange':
                $splitKeysAllWord = implode('|', $splitKeysAll);
                $splitKeysAllWord = '/'.$splitKeysAllWord.'/';
                return $splitKeysAllWord;
                break;
            case 'step1': //分割したキーをarray形式で集める
                $splits = explode(self::$separator['separate'], $splits);
                if(is_array($splits))
                {
                    foreach($splits as $split)
                    {
                        $splitKeysAll[] = $split;
                    }
                }
                else
                {
                    $splitKeysAll[] = $splits;
                }
                return $splitKeysAll;
                break;
            case 'step2':
//				$splitKeysAll = implode('|', $splitKeysAll);
//				$splitKeysAll = '/'.$splitKeysAll.'/';
                return $itemName = preg_split($splitKeysAll, $splits);
                break;
        }
    }

    private function splitSymbols($haystack, $symbol='word', $another=null)
    {
        $needle = self::$separator[$symbol];
        switch($symbol)
        {
            case 'word':
                if($another==self::$separator['bullet']||$another==self::$separator['bullet'])goto ptnA;
                if($another==self::$separator['end']||$another==self::$separator['end'])goto ptnB;
                $result = str_replace($another, $needle.$another, $haystack);
                break;
            case 'separate':
            case 'repeat':
                $result = explode($needle, $haystack);
                break;
            case 'bullet':
                ptnA:
                $result = preg_replace('/^(.*)/', self::$separator['bullet']."$1", $haystack);
                break;
            case 'end':
                ptnB:
                $result = preg_replace('/(.*)$/', "$1".self::$separator['end'], $haystack);
                break;
            default:
                break;
        }
        return $result;
    }

    public function rktReplace($original, $keyword, $role="step1")
    {
        switch($role)
        {
            case 'step1':
                // 分割確認
                list($first, $second) = $this->splitSymbols($keyword['name'], 'separate');
                if(is_null($second)) //分割なし
                {
                    $original =& $this->splitSymbols($original, 'word', $first);
                }
                if(!is_null($second)) //分割あり
                {
                    // 繰り返し確認
                    $repeat1 = $repeat2 = 1;
                    list($word1, $repeat1) = $this->splitSymbols($first, 'repeat');
                    list($word2, $repeat2) = $this->splitSymbols($second, 'repeat');
//					$word1 = preg_quote($word1);
//					$word2 = preg_quote($word2);
//					$original = preg_quote($original);
                    $_shadow1  = $_shadow2 = null;
                    $i = $j = 1;
                    while($i < $repeat1){
                        $_shadow1 .= preg_quote($word1).'.*?';
                        $i++;
                    }
                    while($j < $repeat2){
                        $_shadow2 .= preg_quote($word2).'.*?';
                        $j++;
                    }
                    // マーキング
                    if($word1==self::$separator['bullet'])
                    {
                        $ptn = '/^(.*?'.$_shadow2.preg_quote($word2).')(.*?)/';
                        $original =& preg_replace($ptn, preg_quote(self::$separator['bullet'])."$1".self::$separator['word']."$2", $original);
                    }
                    elseif($word2==self::$separator['end'])
                    {
                        $ptn = '/(.*?'.$_shadow1.')('.preg_quote($word1).'.*?)$/';
                        $original =& preg_replace($ptn, "$1".self::$separator['word']."$2".preg_quote(self::$separator['end']), $original);
                    }
                    else
                    {
                        $ptn = '/(.*?'.$_shadow1.')('.preg_quote($word1).'.*?'.$_shadow2.preg_quote($word2).')(.*?)/';
                        $original =& preg_replace($ptn, "$1".self::$separator['word']."$2".self::$separator['word']."$3", $original);
                    }
                }
                break;
            case 'step2':
                // 分割確認
                $repeat1 = $repeat2 = 1;
                list($first, $second) = $this->splitSymbols($keyword['name'], 'separate');
                if(is_null($second)) //分割なし
                {
//					$original = $first;
                    $original = '/'.preg_quote($first).'(.*?)/';
                }
                if(!is_null($second)) //分割あり
                {
                    // 繰り返し確認
                    list($word1, $repeat1) = $this->splitSymbols($first, 'repeat');
                    list($word2, $repeat2) = $this->splitSymbols($second, 'repeat');
                    if($word1==self::$separator['bullet'])
                    {
                        $original = $repeat1;
//						$keyword = array(preg_quote(self::self::$separator['bullet']));
                        $keyword = '/'.preg_quote('\\').preg_quote(self::$separator['bullet']).'(.*)/';
                        return $keyword;
                    }
                    elseif($word2==self::$separator['end'])
                    {
                        $original = $repeat2;
//						$keyword = array(preg_quote(self::self::$separator['end']));
                        $keyword = '/(.*?)'.preg_quote('\\').preg_quote(self::$separator['end']).'/';
                        return $keyword;
                    }
                    else
                    {
                        $original = ($repeat1 > $repeat2) ? $repeat1 : $repeat2;
//						$original = array($word1, $word2);
                        $original = '/'.preg_quote($word1).'(.*?)'.preg_quote($word2).'/';
                    }
                }
                break;
        }
        return $original;
    }
}
